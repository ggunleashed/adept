package server

import (
	"log"
	"net"
	"net/rpc"
	"reflect"
	"unicode"
	"unicode/utf8"
)

// TODO: Setup server functions. Like: Restart, Close
//       Setup Database functionality and create interface for database classes
//       Add Function in AdeptServer for Initializing Database tables on startup if not existing
//       Add Function in AdeptServer to register only for database
//       Add Config file support
//       Add support for Child microservices
//
//       Add AdeptClient

//AdeptServer ...
type AdeptServer struct {
	Name     string
	Server   *rpc.Server
	Address  string
	Listener net.Listener

	services map[string]*service
}

type service struct {
	name     string
	typ      reflect.Type
	receiver reflect.Value
	methods  map[string]*methodType
}

type methodType struct {
	method    reflect.Method
	ArgType   reflect.Type
	ReplyType reflect.Type
}

// NewAdeptServer ...
func NewAdeptServer() *AdeptServer {
	return &AdeptServer{Name: "Adept Server", Server: rpc.NewServer(), Address: ":8080", services: make(map[string]*service)}
}

// NewAdeptServerWithName ...
func NewAdeptServerWithName(name string) *AdeptServer {
	return &AdeptServer{Name: name, Server: rpc.NewServer(), Address: ":8080", services: make(map[string]*service)}
}

//Start ...
func (adept *AdeptServer) Start() error {
	var err error
	listener, err := net.Listen("tcp", adept.Address)
	if err != nil {
		return err
	}

	log.Printf("Starting %s on: %s", adept.Name, adept.Address)
	log.Println("Services registered:", adept.services)
	adept.accept(listener)
	return nil
}

//Register ...
func (adept *AdeptServer) Register(services ...interface{}) error {
	var err error
	for _, service := range services {
		adept.registerService(service)
		err = adept.Server.Register(service)
		if err != nil {
			return err
		}
	}
	return nil
}

func (adept *AdeptServer) accept(listener net.Listener) {
	for {
		conn, err := listener.Accept()

		if err != nil {
			log.Println("rpc.Serve: accept:", err)
			return
		}
		log.Printf("Accepted connection from: %s", conn.RemoteAddr())
		go adept.Server.ServeConn(conn)
	}
}

func (adept *AdeptServer) registerService(object interface{}) {
	service := new(service)
	service.typ = reflect.TypeOf(object)
	service.receiver = reflect.ValueOf(object)
	serviceName := reflect.Indirect(service.receiver).Type().Name()
	if serviceName == "" {
		log.Printf("Type: %s has no service name\n", service.typ.String())
	}
	if !isExported(serviceName) {
		log.Printf("%s is not exported.\n", serviceName)
	}
	if _, present := adept.services[serviceName]; present {
		log.Printf("%s already defined\n", serviceName)
	}
	service.name = serviceName
	service.methods = suitableMethods(service.typ)
	adept.services[service.name] = service
}

/** Copied Functions from pkg "net/rpc" and the file "server.go"
 ** https://golang.org/src/net/rpc/server.go
 **/
var typeOfError = reflect.TypeOf((*error)(nil)).Elem()

// Is this an exported - upper case - name?
func isExported(name string) bool {
	rune, _ := utf8.DecodeRuneInString(name)
	return unicode.IsUpper(rune)
}

// Is this type exported or a builtin?
func isExportedOrBuiltinType(t reflect.Type) bool {
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	// PkgPath will be non-empty even for an exported type,
	// so we need to check the type name as well.
	return isExported(t.Name()) || t.PkgPath() == ""
}

// suitableMethods returns suitable Rpc methods of typ, it will report
// error using log if reportErr is true.
func suitableMethods(typ reflect.Type) map[string]*methodType {
	methods := make(map[string]*methodType)
	for m := 0; m < typ.NumMethod(); m++ {
		method := typ.Method(m)
		mtype := method.Type
		mname := method.Name
		// Method must be exported.
		if method.PkgPath != "" {
			continue
		}
		// Method needs three ins: receiver, *args, *reply.
		if mtype.NumIn() != 3 {
			continue
		}
		// First arg need not be a pointer.
		argType := mtype.In(1)
		if !isExportedOrBuiltinType(argType) {
			continue
		}
		// Second arg must be a pointer.
		replyType := mtype.In(2)
		if replyType.Kind() != reflect.Ptr {
			continue
		}
		// Reply type must be exported.
		if !isExportedOrBuiltinType(replyType) {
			continue
		}
		// Method needs one out.
		if mtype.NumOut() != 1 {
			continue
		}
		// The return type of the method must be error.
		if returnType := mtype.Out(0); returnType != typeOfError {

			continue
		}
		methods[mname] = &methodType{method: method, ArgType: argType, ReplyType: replyType}
	}
	return methods
}

/** End of Copied functions **/
