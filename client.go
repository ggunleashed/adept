package main

import (
	"log"
	"net"
	"net/rpc"
	"time"
)

/***
*
* THIS IS STILL AN EXAMPLE AND TO TEST. WILL EVENTUALLY BE REMOVED
***/

type Args struct {
	A, B int
}

type Client struct {
	connection *rpc.Client
}

func NewClient(dsn string, timeout time.Duration) (*Client, error) {
	connection, err := net.DialTimeout("tcp", dsn, timeout)
	if err != nil {
		return nil, err
	}
	return &Client{connection: rpc.NewClient(connection)}, nil
}

func main() {
	var counter int32
	client, err := NewClient("localhost:8080", time.Second*2)
	if err != nil {
		log.Println(err)
	} else {
		defer client.connection.Close()
	}
	for {
		if client == nil {
			client, err = NewClient("localhost:8080", time.Second*2)
			if err != nil {
				log.Println(err)
			} else {
				defer client.connection.Close()
			}
		}
		currTime := time.Now()
		if client != nil {
			err = client.connection.Call("Example.IncrementByOne", &counter, &counter)
			if err != nil {
				log.Println(err)
				client = nil
			}
			log.Printf("Result: %d -- Time taken: %s", counter, time.Since(currTime))
			if counter == 2147483647 {
				break
			}
			time.Sleep(time.Millisecond * 2)
		}
	}
}
