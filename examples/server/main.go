package main

import (
	"gitlab.com/ggunleashed/adept/server"
)

func main() {
	adept := server.NewAdeptServer()
	adept.Register(new(Example))
	adept.Start()
}

type Example struct{}

func (e *Example) IncrementByOne(base *int, result *int) error {
	*result = *base + 1
	return nil
}
