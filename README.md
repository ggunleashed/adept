Install by using:
`go get gitlab.com/ggunleashed/adept`


Run server example :
`go run examples/server/main.go`

run client example:
`go run client.go`
